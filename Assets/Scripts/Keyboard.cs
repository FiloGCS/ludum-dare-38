﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyboard : MonoBehaviour {

	public float WAITING_TIME = 3;

	private float timeCount = 0;

	void Update(){
		timeCount += Time.deltaTime;
		if (timeCount > WAITING_TIME) {
			this.GetComponent<AudioSource> ().Play ();
			Destroy (this);
		}
	}
}
