﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrack : MonoBehaviour {

	public Transform focus;
	public Vector3 relativePosition;
	public float responsiveness = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 targetPosition = focus.transform.position + relativePosition;

		this.transform.position = Vector3.Lerp (this.transform.position, targetPosition, Time.deltaTime * responsiveness);
	}
}
