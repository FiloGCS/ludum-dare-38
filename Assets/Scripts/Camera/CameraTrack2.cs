﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CameraTrack2 : MonoBehaviour {

	public Transform focus;
	public Transform pivot;
	public Vector3 relativePosition;
	public float anchorness = 0.5f;
	public float responsiveness = 10f;


	public Transform finishFocus;
	public Vector3 finishRelativePosition;

	private bool RetryRaycast = false;

	void Start(){
		AudioListener.volume = 1;
	}

	void Update () {
		Vector3 targetPosition = (focus.transform.position*(1-anchorness) + pivot.transform.position*anchorness) + relativePosition;
		this.transform.position = Vector3.Lerp (this.transform.position, targetPosition, Time.deltaTime * responsiveness);

		if (RetryRaycast) {
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(ray, out hit, 100.0f)){
				if (hit.collider.gameObject.layer == 5) {
					if (Input.GetMouseButtonDown (0)) {
						//Vuelvo a la escena de inicio
						SceneManager.LoadScene(0);
					}
				}
			}
		}

	}

	public void showFinish(){
		this.focus = finishFocus;
		this.pivot = finishFocus;
		this.relativePosition = finishRelativePosition;
		AudioListener.volume = 0;
		RetryRaycast = true;
	}
}
