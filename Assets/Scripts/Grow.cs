﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grow : MonoBehaviour {

	public float speed;

	private Vector3 targetScale;

	// Use this for initialization
	void Start () {
		this.targetScale = this.transform.localScale;
		this.transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.localScale = Vector3.Lerp (transform.localScale, targetScale, Time.deltaTime* speed);
		if ((transform.localScale - targetScale).magnitude < 0.01f) {
			this.transform.localScale = targetScale;
			Destroy (this);
		}
	}
}
