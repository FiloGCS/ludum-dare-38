﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heat : MonoBehaviour {
	
	public Fan fan;
	public DeployZone deployZone;

	public Gradient coldGradient;
	public Gradient hotGradient;

	public float DAMAGE = 25;
	private ParticleSystem.ColorOverLifetimeModule col;


	void Start(){
		col = GetComponent<ParticleSystem> ().colorOverLifetime;
		col.enabled = true;
		col.color = hotGradient;
		this.GetComponent<Rigidbody> ().AddForce (new Vector3 (Random.Range(0,10),Random.Range(0,10), 0).normalized*Random.Range(100,150));
	}

	void FixedUpdate(){
		if (fan.working) {
			col.color = coldGradient;
			this.GetComponent<Rigidbody> ().AddForce ((fan.transform.position - this.transform.position).normalized * fan.strength);
		} else {
			col.color = hotGradient;
		}
	}

	void OnCollisionEnter(Collision collision){
		this.GetComponent<Rigidbody> ().velocity = Vector3.Reflect(-collision.relativeVelocity,collision.contacts[0].normal);
	}

	void OnTriggerStay(Collider col){
		//FAN
		if (col.gameObject.layer == 13) {
			if (col.GetComponent<Fan> ().working) {
				die ();
			}
		}
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.layer == 14 && !fan.working) {
			deployZone.CONSUMPTION += 1;
			col.transform.parent.GetComponent<Player> ().hurt ();
			Destroy (this.gameObject);
		}
	}

	private void die(){
		Destroy (this.gameObject);
	}
}
