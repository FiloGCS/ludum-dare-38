﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatSpawner : MonoBehaviour {
	
	//LINKS
	public GameObject spawnee;
	public Fan fan;
	public DeployZone deployZone;

	//CONSTANTS
	public float freq;

	//VARIABLES
	private float timeCount= 0;

	void Start () {
		Spawn ();
	}

	void FixedUpdate(){
		timeCount += Time.fixedDeltaTime;
		if (timeCount >= 1 / freq) {
			Spawn ();
			timeCount = 0;
		}
	}

	public void Spawn(){
		if (AudioListener.volume > 0) {
			GameObject newObject = Instantiate (spawnee);
			newObject.transform.position = this.transform.position;
			newObject.GetComponent<Heat> ().fan = fan;
			newObject.GetComponent<Heat> ().deployZone = deployZone;
		}
	}
}
