﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour {

	public FanDeployZone deployZone;

	public float strength = 0.1f;

	public bool working = false;
	private Vector3 currentRotationSpeed;

	private bool wasWorking = false;
	void Update () {
		Vector3 targetRotationSpeed;
		if (deployZone.batteryEnergy > 0) {
			targetRotationSpeed = new Vector3 (0, 0, 10);
			working = true;
		} else {
			targetRotationSpeed = new Vector3 (0, 0, 0.5f);
			working = false;
		}

		this.GetComponent<AudioSource> ().volume = currentRotationSpeed.z / 10;
		this.GetComponent<AudioSource> ().pitch = 1 + currentRotationSpeed.z / 10;

		currentRotationSpeed = Vector3.Lerp (currentRotationSpeed, targetRotationSpeed, Time.deltaTime);
		this.transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles - currentRotationSpeed);
	}
}
