﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	//LINKS
	public GameObject spawnee;
	public Bar progressBar;
	//CONSTANTS
	public float RATE;

	//VARIABLES
	private float timeCount= 0;

	void Start () {
		Spawn ();
	}

	void Update(){
		progressBar.value = (timeCount * RATE * 100);
	}

	void FixedUpdate(){
		timeCount += Time.fixedDeltaTime;
		if (timeCount >= 1 / RATE) {
			Spawn ();
			timeCount = 0;
		}
	}

	public void Spawn(){
		if (this.transform.childCount == 0) {
			GameObject newObject = Instantiate (spawnee);
			newObject.transform.position = this.transform.position;
			newObject.transform.SetParent (this.transform);
		}
	}
}
