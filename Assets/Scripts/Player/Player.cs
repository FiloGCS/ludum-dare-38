﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public DeployZone deployZone;
	public Transform payload;
	public SpriteRenderer eyes;
	public AudioSource hurtAudio;

	public Sprite eyesIdle;
	public Sprite eyesOuch;
	public float ouchDuration;
	private float ouchCounter = 0;

	private GameObject load = null;

	void Start(){
		eyes.sprite = eyesIdle;
	}

	void Update(){
		if (ouchCounter != -1) {
			ouchCounter -= Time.deltaTime;
			if (ouchCounter <= 0) {
				eyes.sprite = eyesIdle;
				ouchCounter = -1;
			}
		}
	}

	//When I touch something
	void OnTriggerEnter (Collider col){
		if (col.gameObject.layer == 9 && load == null) {
			//BATTERY
			payload.transform.position = col.transform.position;
			load = col.gameObject;
			load.GetComponent<Collider> ().enabled = false;
			col.transform.SetParent (payload);
		} else if (col.gameObject.layer == 8 && load != null) {
			//DEPLOY ZONE
			if (col.gameObject.GetComponent<DeployZone> ().Deploy (load)) {
				load = null;
			}
		} else if (col.gameObject.layer == 15 && load != null) {
			//FAN DEPLOY ZONE
			if (col.gameObject.GetComponent<FanDeployZone> ().Deploy (load)) {
				load = null;
			}
		}
	}


	public void hurt(){
		eyes.sprite = eyesOuch;
		ouchCounter = ouchDuration;
		hurtAudio.pitch = deployZone.CONSUMPTION/20 + 0.8f;
		hurtAudio.Play ();
	}


}
