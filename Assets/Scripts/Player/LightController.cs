﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

	public float MIN_INTENSITY = 1;
	public float MAX_INTENSITY = 1.2f;
	public float AMPLITUDE = 0.02f;
	public float FREQUENCY = 4;

	private Light light;
	private Vector3 targetPosition;
	private float timeCount = 0;

	void Start(){
		light = this.GetComponent<Light> ();
	}

	void Update () {
		light.intensity = Random.value * (MAX_INTENSITY - MIN_INTENSITY) + MIN_INTENSITY;
		timeCount += Time.deltaTime;
		if (timeCount >= 1/FREQUENCY) {
			targetPosition = new Vector3 (Random.value * AMPLITUDE - AMPLITUDE/2, Random.value * AMPLITUDE - AMPLITUDE/2, 0);
			timeCount = 0;
		}
		this.transform.localPosition = Vector3.Lerp (transform.localPosition, targetPosition, Time.deltaTime*5);
	}
}
