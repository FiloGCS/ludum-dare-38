﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public Fan fan;

	public float MIN_INPUT_THRESHOLD = 0.05f;

	public float gravity = 3f;
	public float speed = 0.1f;
	public float jumpSpeed = 1f;
	public int jumpFrames = 3;

	private CharacterController cc;

	private float vSpeed;

	private int jump = 0;

	// Use this for initialization
	void Start () {

		Application.targetFrameRate = 60;

		cc = this.transform.GetComponent<CharacterController> ();
		vSpeed = 0;
	}

	void Update (){
		if (jump > 0) {
			jump--;
		}
		if (Input.GetButtonDown ("Jump") && cc.isGrounded) {
			jump = jumpFrames;
		}
		if (Input.GetButtonUp("Jump")) {
			jump = 0;
		}
	}

	void FixedUpdate () {
		//Read Input
		float hInput = Input.GetAxis ("Horizontal");
		if (Mathf.Abs(hInput) < MIN_INPUT_THRESHOLD)hInput = 0;

		//Calculate vertical Velocity
		//If I'm jumping
		if (jump>0){
			//Jump
			vSpeed = jumpSpeed ;
		//If I'm touching the ground
		}else if(cc.isGrounded){
			//Stop moving vertically
			vSpeed = -0.1f;
		//If I'm in free fall
		}else{
			if (cc.velocity.y == 0) {
				vSpeed = 0;
			}
			//Accelerate taking gravity into acount
			vSpeed -= gravity * Time.fixedDeltaTime;
		}

		//Move
		cc.Move (new Vector3 (hInput, vSpeed, 0f)*speed);

		//Reset control bools
	}

}
