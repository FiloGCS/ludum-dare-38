﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour {

	public float MAX_VALUE = 100;
	public float value;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float yPosition = (value / MAX_VALUE)/2-0.5f;
		float yScale = (value / MAX_VALUE);
		this.transform.localPosition = new Vector3 (0, yPosition, 0);
		this.transform.localScale = new Vector3 (1, yScale, 1);
	}
}
