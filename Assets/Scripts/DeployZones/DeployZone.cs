﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployZone : MonoBehaviour {

	//LINKS
	public Transform loader; //Receives the batteries
	public Transform energyBar;
	public Transform consumptionBar;
	public Rng rng;
	public Camera camera;

	//CONSTANTS
	public Color WAITING_COLOR;
	public Color LOADING_COLOR;
	public float LOADING_TIME;
	public float MAX_ENERGY;
	public float CONSUMPTION;

	//VARIABLES
	public float energy = 100;
	private float lastEnergy = 100;
	public float batteryEnergy = 0;
	private bool loading = false;


	void Update(){
		//UI Update
		energyBar.GetComponent<Bar>().value = Mathf.Max(0,energy);
		consumptionBar.GetComponent<Bar>().value = Mathf.Min(Mathf.Max(0,CONSUMPTION*4),100);
	}

	void FixedUpdate () {
		//If we're using the battery to restore energy
		if (loading) {
			//If we're not done
			if (batteryEnergy > 0) {
				//Calculate the energy to transfer this frame
				float energyTransfer = (MAX_ENERGY / LOADING_TIME) * Time.fixedDeltaTime;
				//Transfer it
				energy += energyTransfer;
				batteryEnergy -= energyTransfer;
				//Has anyone capped?
				if (energy > MAX_ENERGY) energy = MAX_ENERGY;
				if (batteryEnergy < 0) batteryEnergy = 0;
			}
			//If the battery is depleted
			if(batteryEnergy == 0){
				//We're not loading anymore
				loading = false;
				this.GetComponent<Light> ().color = WAITING_COLOR;
				//Throw that battery away!
				Destroy(loader.GetChild(0).gameObject);
			}
		}

		//Consume Energy
		energy -= CONSUMPTION * Time.fixedDeltaTime;

		if (energy < 20 && lastEnergy >=20) {
			rng.shoutNo (0);
		}

		if (energy < 8 && lastEnergy >= 8) {
			rng.shoutNo (1);
		}

		if (energy <= 0) {
			energy = 0;
			camera.GetComponent<CameraTrack2> ().showFinish ();
		}
		lastEnergy = energy;
	}
		
	public bool Deploy(GameObject payload){
		//Am I free to take it?
		if(!loading){
			//Feedback blip
			if (this.GetComponent<AudioSource> () != null) {
				this.GetComponent<AudioSource> ().Play ();
			}
			//Let's bring that battery home
			loader.transform.position = payload.transform.position;
			payload.transform.SetParent (loader);
			//UI Light change color to "LOADING"
			this.GetComponent<Light> ().color = LOADING_COLOR;
			//We're indeed loading
			loading = true;
			//And the battery is full, not for long though ;)
			batteryEnergy = 100;
			return true;
		}
		return false;
	}
}
