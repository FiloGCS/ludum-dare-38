﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanDeployZone : MonoBehaviour {

	//LINKS
	public Transform loader; //Receives the batteries

	//CONSTANTS
	public Color WAITING_COLOR;
	public Color LOADING_COLOR;
	public float LOADING_TIME;
	public float MAX_ENERGY;

	//VARIABLES
	public float batteryEnergy = 0;
	private bool loading = false;

	void FixedUpdate () {
		//If we're using the battery to restore energy
		if (loading) {
			//If we're not done
			if (batteryEnergy > 0) {
				//Calculate the energy to transfer this frame
				float energyTransfer = (MAX_ENERGY / LOADING_TIME) * Time.fixedDeltaTime;
				//Transfer it
				batteryEnergy -= energyTransfer;
				//Has anyone capped?
				if (batteryEnergy < 0) batteryEnergy = 0;
			}
			//If the battery is depleted
			if(batteryEnergy == 0){
				//We're not loading anymore
				loading = false;
				this.GetComponent<Light> ().color = WAITING_COLOR;
				//Throw that battery away!
				Destroy(loader.GetChild(0).gameObject);
			}
		}
	}
		
	public bool Deploy(GameObject payload){
		//Am I free to take it?
		if(!loading){
			//Feedback blip
			if (this.GetComponent<AudioSource> () != null) {
				this.GetComponent<AudioSource> ().Play ();
			}
			//Let's bring that battery home
			loader.transform.position = payload.transform.position;
			payload.transform.SetParent (loader);
			//UI Light change color to "LOADING"
			this.GetComponent<Light> ().color = LOADING_COLOR;
			//We're indeed loading
			loading = true;
			//And the battery is full, not for long though ;)
			batteryEnergy = 100;
			return true;
		}
		return false;
	}
}
