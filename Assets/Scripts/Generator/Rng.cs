﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rng : MonoBehaviour {

	//LINKS
	public DeployZone deployZone;
	public TextMesh display;
	public TextMesh legendaryCounter;
	public AudioSource dropSound;

	//CONSTANTS
	public int MIN = 0;
	public int MAX = 100;
	public float RATE;
	public float CONSUMPTION = 5;

	public List<AudioClip> yesSounds;
	public List<AudioClip> noSounds;

	//VARIABLES
	public int value;
	public int legendaryCount = 0;
	private float timeCount= 0;

	void FixedUpdate () {
		timeCount += Time.fixedDeltaTime;
		//Is it time to generate another number?
		if (timeCount > 1/RATE) {
			value = Random.Range (Mathf.Max(0,MIN + (int) deployZone.energy/3), MAX);
			if (value > 85) {
				dropLegendary ();
				if (!this.GetComponent<AudioSource> ().isPlaying) {
					shoutYes ();
				}
				display.text = "-" + value.ToString () + "-";
			} else {
				display.text = value.ToString();
			}
			//Reset the timer for the next number
			timeCount = 0;
		}
	}

	private void dropLegendary(){
		if (AudioListener.volume != 0) {
			legendaryCount++;
			legendaryCounter.text = legendaryCount.ToString ();
			dropSound.Play ();
		}
	}

	public void shoutYes(){
		if (Time.timeSinceLevelLoad > 4) {
			this.GetComponent<AudioSource>().clip = yesSounds[Random.Range (0, yesSounds.Count)];
			this.GetComponent<AudioSource> ().Play ();
		}
	}

	public void shoutNo(){
		if (!this.GetComponent<AudioSource> ().isPlaying) {
			this.GetComponent<AudioSource> ().clip = noSounds [Random.Range (0, noSounds.Count)];
			this.GetComponent<AudioSource> ().Play ();
		}
	}

	public void shoutNo(int n){
		this.GetComponent<AudioSource> ().Stop ();
		this.GetComponent<AudioSource> ().clip = noSounds[n];
		this.GetComponent<AudioSource> ().Play ();
	}
}
