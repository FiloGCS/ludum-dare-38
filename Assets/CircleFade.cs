﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleFade : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad < 1) {
			this.GetComponent<Renderer>().material.SetFloat("_Cutoff", Mathf.Min(Time.timeSinceLevelLoad,1));
		}
	}
}
